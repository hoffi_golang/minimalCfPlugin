package main

import (
	"code.cloudfoundry.org/cli/plugin"
	"gitlab.com/hoffi_golang/minimalCfPlugin/commands"
)

func main() {
	plugin.Start(new(commands.MinimalCfPlugin))
}
