package commands

import (
	"fmt"

	"code.cloudfoundry.org/cli/plugin"
)

// Command1 of this plugin
func (c *MinimalCfPlugin) Command1(cliConnection plugin.CliConnection) {
	fmt.Println("Function command-1 in plugin 'MinimalCfPlugin' is called.")
	output, err := cliConnection.ApiEndpoint()
	failOnError(err, "Error getting targeted ApiEndpoint")
	fmt.Println("ApiEndpoint: ", output)
	output, err = cliConnection.AccessToken()
	failOnError(err, "Error getting targeted AccessToken")
	fmt.Println("AccessTocken:", output)
	org, err := cliConnection.GetCurrentOrg()
	failOnError(err, "Error getting targeted GetCurrentOrg")
	fmt.Println("ORG:         ", org)
	space, err := cliConnection.GetCurrentSpace()
	failOnError(err, "Error getting targeted GetCurrentSpace")
	fmt.Println("Space:       ", space)
	fmt.Println("Function command-1 in plugin 'MinimalCfPlugin' finished.")
}
