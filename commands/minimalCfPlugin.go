package commands

import "code.cloudfoundry.org/cli/plugin"

// MinimalCfPlugin type
type MinimalCfPlugin struct{}

// GetMetadata for this plugin
func (c *MinimalCfPlugin) GetMetadata() plugin.PluginMetadata {
	return plugin.PluginMetadata{
		Name: "MinimalCfPlugin",
		Version: plugin.VersionType{
			Major: 0,
			Minor: 1,
			Build: 0,
		},
		MinCliVersion: plugin.VersionType{
			Major: 6,
			Minor: 24,
			Build: 0,
		},
		Commands: []plugin.Command{
			{
				Name:     "command-1",
				HelpText: "Help text for command-1",
				UsageDetails: plugin.Usage{
					Usage: "command-1 - no real functionality\n   cf command-1",
				},
			},
			{
				Name:     "command-2",
				HelpText: "Help text for command-2",
			},
			{
				Name:     "command-3",
				HelpText: "Help text for command-3",
			},
		},
	}
}

// Run this plugin
func (c *MinimalCfPlugin) Run(cliConnection plugin.CliConnection, args []string) {
	if args[0] == "command-1" {
		c.Command1(cliConnection)
	} else if args[0] == "command-2" {
		c.Command2(cliConnection)
	} else if args[0] == "command-3" {
		c.Command3()
	}
}
