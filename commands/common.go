package commands

import (
	"fmt"
	"log"
	"os"
)

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func exit1(err string) {
	fmt.Println("FAILED\n" + err)
	os.Exit(1)
}

func dumpOutput(output []string) {
	fmt.Println("")
	fmt.Println("---------- Command output from the plugin ----------")
	for index, val := range output {
		fmt.Printf("#%5d:%v\n", index, val)
	}
	fmt.Println("----------              FIN               -----------")
}
