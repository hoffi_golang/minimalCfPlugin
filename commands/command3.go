package commands

import "fmt"

// Command3 of this plugin
func (c *MinimalCfPlugin) Command3() {
	fmt.Println("Function command-3 in plugin 'MinimalCfPlugin' is called.")
}
