package commands

import (
	"encoding/json"
	"fmt"
	"strings"

	"code.cloudfoundry.org/cli/plugin"
)

// AppsModel corresponds to the json returned by `cf curl v2/apps`
type AppsModel struct {
	NextURL   string     `json:"next_url,omitempty"`
	Resources []AppModel `json:"resources"`
}

// AppModel corresponds to the entity element returned within a `cf curl v2/apps`
type AppModel struct {
	Entity EntityModel `json:"entity"`
}

// EntityModel name and state for an app entity returned within a `cf curl v2/apps`
type EntityModel struct {
	Name  string `json:"name"`
	State string `json:"state"`
}

// Command2 of this plugin
func (c *MinimalCfPlugin) Command2(cliConnection plugin.CliConnection) {
	fmt.Println("Function command-2 in plugin 'MinimalCfPlugin' is called.")
	allApps, err := getAllApps(cliConnection)
	if err != nil {
		exit1("Error curling v2/apps: " + err.Error())
	}

	fmt.Printf("Listing apps @ endpoint v2/apps...\n\n")
	for _, app := range allApps.Resources {
		fmt.Println(app.Entity.Name, app.Entity.State)
	}

}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

func getAllApps(cliConnection plugin.CliConnection) (AppsModel, error) {
	nextURL := "v2/apps"
	allApps := AppsModel{}

	for nextURL != "" {
		output, err := cliConnection.CliCommandWithoutTerminalOutput("curl", nextURL)
		if err != nil {
			return AppsModel{}, err
		}
		// dumpOutput(output)

		fmt.Println("json unmarshalling", nextURL)
		apps := AppsModel{} // unmarshalling
		err = json.Unmarshal([]byte(strings.Join(output, "")), &apps)
		if err != nil {
			return AppsModel{}, err
		}
		fmt.Println("unmarshalled.", nextURL)

		allApps.Resources = append(allApps.Resources, apps.Resources...)

		nextURL = apps.NextURL
	}

	return allApps, nil
}
